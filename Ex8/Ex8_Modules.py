############ Example 8 - Python Modules #######################
#Modules can define functions, variables and classes 

#Example 8.1 
print ("Ex 8.1")
#Importing a module effectively runs all code (See directory contents Import-ant.py)

#Python modules for import must have the .py suffix
import Important                #When a module is imported, all code is executed in sequence, as in this .py script
                                 #The print statment in the imported module is executed.


#Example 8.2
print ("\n\nEx 8.2")
#Calling funtions embedded within imported modules
Time=Important.Timestamp() #Object oriented = Access functions and variables using the . [DOT] operator e.g. Module.Function(Argument1,Argument2) or Module.VariableName

#Example 8.3
print("\n\nEx 8.3")
#Using a variable which is assigned in an imported module
mystring=(Important.dummyxml)
print (mystring)


#Example 8.4 Selective variable import losing module.object association
print ("\n\nEx 8.4")
from Important import gobbledegook as gibberish
print (gibberish)

#Example 8.5 Selective function import losing module.object association
print ("\n\nEx 8.5")
from Important import Timestamp as Timefunction
Timefunction()


######### Useful library modules ###############

# Python comes with many useful library modules, which you can just import and use as needed
# Particularly useful

#import time              we have already used this to generate timestamps
#import os                many useful bash commands can be executed using os.system("bash command string here")
#import sys               python module search path & system platform variables e.g. operating system, memory etc 

################################################
