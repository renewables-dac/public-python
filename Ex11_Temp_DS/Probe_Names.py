# Use this file to keep track of your DS18B20 probe hardware names.
#Each probe has a 64 bit hardware name like 28-000000xxxxxxx

#You will need to connect your probes to your 1 wire bus, then check the folder:
#
#   /sys/bus/w1/devices
#
#   Make a note of them, label your probes and copy the hardware addresses to these variables:


T1_ID="28-0000065bf76c"
T2_ID="28-0000065bf76c" 




#example probe HW name:
T_Dummy_ID="28-0000065bf76c"

