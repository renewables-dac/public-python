#File handling examples
#Common file handling operations include:
#Read, Write, Append 'r', 'w', 'a'


#Example 5.1 reading a file into a string
print ("Ex5.1")
filename="Datafile_1" #provide filename (omit path for current working directory)
input=open(filename,'r') #create input file 'r' = read
mystring=input.read() #read entire file into single string
input.close()
print(mystring)       
# ++view the contents of Datafile_1, and edit it, or create a new directory (mkdir in the shell) with a new data file and
# ++try to read the new file using the relative path to your new directory




#Example 5.2 writing a string to a file
print ("\nEx 5.2")
filename="Datafile_2"
output=open(filename,"w") #create file for output
import time
timestamp=time.strftime("%Y-%m-%d %H:%M")
output.write("This file was created at "+ timestamp) #actually write a string to the file
output.close()
print ("File creation recorded in Datafile_2") 
#check the contents of Datafile_2 by using the cat command in the shell




#Example 5.3 APPENDING a string to a file
print ("\nEx 5.3")# This is most useful when creating data files which arise from polling sensors
filename="Datafile_3"
append_my_freakin_file=open(filename,'a') #you can call the input or output file handle whatever you like...
append_my_freakin_file.write("\n Spam "+timestamp)
append_my_freakin_file.close() 
print ("Datafile_3 appended")
#Display Datafile3 using cat, run this script a few more times, then check again.





############################### Key points for file handling ############################

#1) Consider whether you need to provide a full file path (best if running using crontab)
#2) Use open(filename,'r') for read input, 'w' for overwrite, or 'a' to append a file
#3) Close the file once finished: filehandle.close()

#########################################################################################

############### Displaying files after script has run ############
import os                                    #NOTE module os enables shell commands os.system("shell/command/here")
print ("\n\n\n\nContents of Datafile_1: \n")
os.system ("cat Datafile_1")
print ("\nContents of Datafile_2: \n")
os.system ("cat Datafile_2")
print ("\nContents of Datafile_3: \n")
os.system ("cat Datafile_3")
##########################################


