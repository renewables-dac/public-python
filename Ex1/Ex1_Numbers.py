#Example 1 - ha.(Similarly to MatLab)ndling numerical variables

#There is no need to define variables before using them. They are created when first assigned a value

#Precedence - use brackets to avoid having to memorise precedence rule:
#Ex 1.1
print("Ex1.1")
a=(2+3*4)
print (a)
#notice how multiplication takes precedence


#usual math operators + - * / are used. Some slightly different, but useful operators:

#Powers **
#Ex1.2
print("\nEx1.2")
print (2**8)
print(2^8) #the ^ operator is for bitwise comparison and not essential for this module. Just don't get it confused with **

#Complex numbers j or J (upper or lowercase may be used to denote img part)
#Ex1.3
print("\nEx1.3")
b=1j*1j
print (b)
c=5+2j
d=4+3j
e=c/d
print (e)

#Hexadecimal values always have prefix 0x  remember base 16 0-9, then a-f can mix upper / lower caSE
#Ex1.4
print ("\nEx1.4")
f=0xfFFF
print(f)

#Ex 1.5
print("\nEx1.5")
#math module is REQUIRED for system values of pi and e, also for TRIG functions
import math

print (math.pi)
print (math.e)
print (math.cos(math.pi))

#Ex 1.6
print ("Ex1.6")
#Rounding, Integers, Absolute values
print(round(math.pi,4)) #argument of round function round(number,decimal places)
print(round(math.pi)) #default dp =0
print(int(math.e)) # note int truncates the decimal places without rounding
print(abs(-44326371846))
