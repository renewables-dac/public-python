######################################################### No need to modify this part ##############################################

from ABE_ADCPi import ADCPi           #You can treat this module as a black box. Try to understand the examples of what to put in as arguments, and what to expect as outputs...
                                      #Remember, if you recycle this code, you will need to copy the imported ABE_ADCPi module into the same folder as your new script
import smbus
bus=smbus.SMBus(1)    #I2C bus 1 for latest Raspberry-Pis

address_1_4=0x68      #I2C addresses for each of the 4 channel ADCs on the board. (Default 0x68, 0x69)
address_5_8=0x69      #Find these out using $i2cdetect -y 1 and remember if you need more channels, the boards are stackable, with address pin selection (see data sheet)


adc = ADCPi(bus, address_1_4, address_5_8, 18) #this configures the ADC addresses, bit rate 



###################################################################### JUICY STUFF #########################################################################


############# Set the programmable GAIN Pre-ADC Amplifier ###########
#Ex13.1
print "Ex13.1"
gain=1                     #Gain values can be x1 x2 x4 or x8       if you need more gain, you'll have to build another amp into your signal conditioning circuit
adc.set_pga(gain)


##########   Set bit rate

#	12 = 12 bit (240SPS max)        ** SPS - "Samples per second"
#	14 = 14 bit (60SPS max)
#	16 = 16 bit (15SPS max)
#	18 = 18 bit (3.75SPS max)       Higher bit rates reduce speed 
rate=18
adc.setBitRate(rate)

############ Select Channel

channel=1                    #channel can be 1 to 8

#Output can be as a bin number

#raw=adc.read_raw(channel)
#print (raw)

#Or as a voltage
voltage = adc.read_voltage(channel)
print (voltage)

#Either voltage or raw values are fine, as you are more than likely going to convert the ADC output back into the variable you are measuring using your calibration equation parameters

#Pitfalls:
# Make sure your measured voltages are w.r.t. a commmon ground at zero volts.

